<?php 
    include_once ('../src/QuanttyManager.php');
   $show = new QuanttyManager();
   $shown = $show->index();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            table{
                 border: 1px solid black;
            } 
            table, thead{
                border-bottom: 1px solid black;
                border-collapse: collapse;
            }
               table, tfoot{
                border: 1px solid black;
                border-collapse: collapse;
            }
        </style>
    </head>
    <body>
        <a href="create.php">Add</a>
        <table>
            <thead>
                <tr>
                    <th>
                        Weekday
                    </th>
                    <th>
                        Date
                    </th>
                    <th>
                        Manager
                    </th>
                    <th>
                        Qty
                    </th>
                </tr>
            </thead>
            <tbody>
               
                <?php
               // $slno =1;
               foreach($shown as $all){
               ?>
                <tr>
                    <td>
                       <?php echo $all->id; ?>
                    </td>
                    <td>
                      <?php echo $all->weekday; ?>
                    </td>
                    <td>
                       <?php echo $all->date; ?>
                    </td>
                    <td>
                        <?php echo $all->qty; ?>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3">Total</td>
                    <td>52314</td>
                </tr>
            </tfoot>
        </table>

    </body>
</html>
